<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payments;

use Anand\LaravelPaytmWallet\Facades\PaytmWallet;

use Validator, Exception;

use Razorpay\Api\Api;

use Session;

use Redirect;

use Illuminate\Support\Facades\Input;



class PaymentController extends Controller
{
    //
    // display a form for payment
    public function initiate()
    {
        return view('paytm');
    }

    public function pay(Request $request)
    {

        $amount = 1500; //Amount to be paid

         $validator = Validator::make($request->all() , [
            'mobile' => 'digits_between:6,13',
            'email' => 'required|max:255',
            ]);

        if($validator->fails()) {

            $errors = implode(',',$validator->messages()->all());

            return back()->with('messages', $errors);
        }

        $userData = [
            'name' => $request->name, // Name of user
            'mobile' => $request->mobile, //Mobile number of user
            'email' => $request->email, //Email of user
            'fee' => $amount,
            'order_id' => $request->mobile . "_" . rand(1, 1000) //Order id
        ];

        $paytmuser = Payments::create($userData); // creates a new database record

        $payment = PaytmWallet::with('receive');

        $payment->prepare([
            'order' => $userData['order_id'],
            'user' => $paytmuser->id,
            'mobile_number' => $userData['mobile'],
            'email' => $userData['email'], // your user email address
            'amount' => $amount, // amount will be paid in INR.
            'callback_url' => route('status') // callback URL
        ]);
        return $payment->receive();  // initiate a new payment
    }

    public function paymentCallback()
    {

        try {

            $transaction = PaytmWallet::with('receive');

            $response = $transaction->response();

            $order_id = $transaction->getOrderId(); // return a order id

            $transaction->getTransactionId(); // return a transaction id

            // update the db data as per result from api call
            if ($transaction->isSuccessful()) {

                Payments::where('order_id', $order_id)->update(['status' => 1, 'transaction_id' => $transaction->getTransactionId()]);

                return redirect(route('initiate.payment'))->with('message', "Your payment is successfull.");
         
            } else if ($transaction->isFailed()) {

                Payments::where('order_id', $order_id)->update(['status' => 0, 'transaction_id' => $transaction->getTransactionId()]);

                return redirect(route('initiate.payment'))->with('message', "Your payment is failed.");
           
            } else if ($transaction->isOpen()) {

                Payments::where('order_id', $order_id)->update(['status' => 2, 'transaction_id' => $transaction->getTransactionId()]);

                return redirect(route('initiate.payment'))->with('message', "Your payment is processing.");
            }
            $transaction->getResponseMessage(); //Get Response Message If Available

            // $transaction->getOrderId(); // Get order id

        } catch (Exception $e) {

            return back()->with('flash_error', $e->getMessage());
        }
    }


      
    public function pay_money_by_razorpay(Request $request){
         
        $data['amount'] = $request->amount ? : '1000.00';

        $data['user_id'] = $request->user_id ?? 5;

        return view('razorpay')->with('data', $data);

    }


    public function razorpay_payment(Request $request)
    {
        //Input items of form
        $input = $request->all();
        //get API Configuration 
        $api = new Api(config('razorpay.razor_key'), config('razorpay.razor_secret'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if(count($input)  && !empty($input['razorpay_payment_id'])) {

            try {

                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 

            } catch (\Exception $e) {

                return  $e->getMessage();

                \Session::put('error',$e->getMessage());

                return redirect()->back();
            }


            $payment = new \App\RazorPayments;

            $payment->id = false;

            $payment->transaction_id =  $response->id;

            $payment->card_id =  $response->card_id;

            $payment->amount =  $response->amount/100 ?? 0.00;

            $payment->status =  $response->status ?? '';

            $payment->order_id = $request->order_id;

            $payment->user_id =  $request->user_id ?: 0;

            $payment->save();

            return view('razorpay-payment-success');
        }
        else{

            return redirect()->route('payment.failure');

        }
        
        \Session::put('success', 'Payment successful, your order will be despatched in the next 48 hours.');

        return redirect()->back();
    }



    public function failure(Request $request){

        return view('razorpay-payment-failure');
    }



    
}
