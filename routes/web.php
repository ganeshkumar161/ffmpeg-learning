<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('upload', 'UploadController@upload');

Route::post('text_upload', 'UploadController@video_watermark_text');

Route::post('text_upload_animate', 'UploadController@video_watermark_text_animate');

Route::post('merge_video', 'UploadController@merge_video_validation');

Route::get('/initiate','PaymentController@initiate')->name('initiate.payment');

Route::post('/payment','PaymentController@pay')->name('make.payment');

Route::post('/payment/status', 'PaymentController@paymentCallback')->name('status');


Route::get('pay_money_by_razorpay', 'PaymentController@pay_money_by_razorpay')->name('pay_money_by_razorpay');

Route::post('razor_payment', 'PaymentController@razorpay_payment')->name('razor.payment');

Route::get('razorpay/failure', 'RazorpayController@failure')->name('razorpay_payment.failure');
