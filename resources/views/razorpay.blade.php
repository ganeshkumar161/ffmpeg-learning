<style type="text/css">
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: "Quicksand", sans-serif;
}
body{
  background:#ecf0f1
}
.mainContainer {
  width: 700px;
  margin: 0 auto;
  box-shadow:0 0 10px #2323;
  margin-top: 50px;
  background:white;
}
.cardHolder {
  height: 300px;
  background: linear-gradient(
    to right,
    #6480ef,
    #7775e8,
    #806ce8
  ); 
/*   border-bottom-left-radius:50%;
  border-bottom-right-radius:50%; */
}
.center{
  display:flex;
  justify-content:center;
  align-items:center;
}
.vcenter{
  display:flex;
  align-items:center;
}
.heading{
  height:40px;
  font-size:14px;
  letter-spacing:2px;
  color:white;
}
.stepHeading{
  height:20px;
  font-size:16px;
  font-weight:bolder;
  letter-spacing:2px;
  color:white;
}
.card{
  height:200px;
  width:400px;
  background:white;
  margin:0 auto;
  border-radius:1.2em;
  margin-top:25px;
  padding:12px 20px;
}
.card > .part{
  height:50px;
  margin-bottom:4px;
}
.card > .top{
  display:flex;
  justify-content:flex-end;
}
.card > .top > img{
  height:48px;
}
.infoheader{
  height:20px;
  font-size:11px;
  font-weight:bold;
  letter-spacing:2px;
  color:#95a5a6
}
.card > .middle > .infocontent{
  justify-content:space-between;
  height:30px;
}
.number{
  width:150px;
}
.card > .middle > .infocontent > .num{
  height:25px;
  font-size:14px;
  letter-spacing:2px;
  font-weight:bolder;
}
.card > .bottom{
  display:flex;
  justify-content:space-between
}
.holderInfo{
  width:150px;
}
.holderInfo > .name,.expDate > .date{
  font-size:12px;
  font-weight:bolder;
  letter-spacing:2px;
}
.status{
  height:35px;
  width:300px;
  margin:0 auto;
  box-shadow:0 0 10px #2323; 
  margin-top:16px;
  padding:0 16px;
  font-size:12px;
  letter-spacing:2px;
  font-weight:bolder;
}
.status > i{
  color:#16a085;
  margin-right:20px;
  font-size:16px;
}
h5{
  width:220px;
  margin:0 auto;
  padding:8px;
  margin-top:14px;
  border-bottom:2px solid #bdc3c7;
  letter-spacing:2px;
  font-size:12px;
  margin-bottom:12px;
}
.options{
  width:250px;
  height:75px;
  margin:0 auto;
  justify-content:space-between;
}
.options > .opt{
  height:75px;
  width:75px;
  box-shadow:0 0 10px #2323; 
  border-radius:5px;
  display:flex;
  flex-direction:column;
  justify-content:center;
}
.options > .opt > .icon{
  height:30px;
  color:#95a5a6;
  font-size:20px;
}
.optname{
  height:30px;
  font-size:10px;
  color:#232323;
  letter-spacing:2px;
  font-weight:bolder
}
.payment{
  height:80px;
  position:relative;
  top: 24px;
  box-shadow: 0 -5px 5px -5px #2323;
  justify-content:space-around;
}
.val{
  height:40px;
  font-size:24px;
  font-weight:bolder;
  letter-spacing:2px;
  color:#6480ef;
}
.razorpay-payment-button{
  width:163px;
  height:35px;
  border-radius:1.2em;
  background:linear-gradient(
    to right,
    #6480ef,
    #7775e8,
    #806ce8
  ); 
  color:white;
  font-weight:bolder;
  letter-spacing:2px;
}


</style>








<div class="mainContainer">
  <div class="cardHolder">
    <div class="header">
      <div class="heading center">CHECKOUT</div>
      <div class="stepHeading center">Payment Method</div>
      <div class="card">
        <div class="top part">
        </div>
 
    <div class="amount" >
      <div class="infoheader vcenter" style="margin-left:36%">Total Amount</div>
      <div class="infocontent val vcenter" style="margin-left:36%">₹ 100</div>    
    </div>
    {!! Session::forget('error') !!}
            @if($message = Session::get('success'))
                <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Success!</strong> {{ $message }}
                </div>
            @endif
            {!! Session::forget('success') !!}
            <div class="panel panel-default">

                <div class="panel-body text-center">
                    <form action="{!!route('razor.payment')!!}" method="POST" >
                        <!-- Note that the amount is in paise = 50 INR -->
                        <!--amount need to be in paisa-->
                        <script src="https://checkout.razorpay.com/v1/checkout.js"
                                data-key="{{ Config::get('razorpay.razor_key') }}"
                                data-amount="{{$data['amount']}}"
                                data-merchant_order_id="1"
                                data-buttontext="Pay ₹ 100"
                                data-name="CGSTORE"
                                data-user="1"
                                data-description="Order Value"
                                data-prefill.name="kumar"
                                data-prefill.email="kumar@gmail.com"
                                data-image="https://hotel-backend.codegama.net/logo.png"
                                data-theme.color="#528FF0">
                        </script><br>
                        <input type="hidden" name="user_id" value="{{$data['user_id']}}">
                        <input type="hidden" name="order_id" value="1">
                        <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                    </form>
                </div>
            </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
//  $('.mainContainer').hide();
//  $('.razorpay-payment-button').trigger('click');
</script>

<style type="text/css">
#header {
    background: #002952 !important;
    color: #FFFFFF;
}

.razorpay-payment-button{
    margin-left:30%;
}
</style>