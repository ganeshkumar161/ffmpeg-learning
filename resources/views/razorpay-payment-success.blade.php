

<style type="text/css">
.success-page{
  max-width:300px;
  display:block;
  margin: 0 auto;
  text-align: center;
  position: relative;
  top: 50%;
  transform: perspective(1px) translateY(50%)
}
.success-page img{
  max-width:62px;
  display: block;
  margin: 0 auto;
}

.btn-view-orders{
  display: block;
  border:1px solid #47c7c5;
  width:100px;
  margin: 0 auto;
  margin-top: 45px;
  padding: 10px;
  color:#fff;
  background-color:#47c7c5;
  text-decoration: none;
  margin-bottom: 20px;
}
.pay_text_css{
  color:#47c7c5;
  margin-top: 25px;

}
.alert {
  background: #FAE6E6;
  padding: .5em 1em 2em 2em;
  margin: 0 0 0 0;
  box-shadow: inset 6px 0 #CC0000;
}

.pay_fail_text_css {
  font-size: 1.5em;
  font-weight: 700;
  color: black;
  letter-spacing: 0.02em;
  margin-top: 1em;
  margin-left: 2em;
  margin-bottom: .5em;
}

.alert_text {
  margin-left: 3em;
  margin-bottom: .5em;
}
</style>


<div class="success-page">
   <img  src="" class="center" alt="" />
  <h2 class="pay_text_css">Payment Successful !</h2>
  <p>We are delighted to inform you that we received your payments</p>
  <a href="#" class="btn-view-orders">View Orders</a>
</div>
</div>

