<style type="text/css">
.success-page{
  max-width:300px;
  display:block;
  margin: 0 auto;
  text-align: center;
  position: relative;
  top: 50%;
  transform: perspective(1px) translateY(50%)
}
.success-page img{
  max-width:62px;
  display: block;
  margin: 0 auto;
}

.btn-view-orders{
  display: block;
  border:1px solid #47c7c5;
  width:100px;
  margin: 0 auto;
  margin-top: 45px;
  padding: 10px;
  color:#fff;
  background-color:#47c7c5;
  text-decoration: none;
  margin-bottom: 20px;
}
.pay_text_css{
  color:#47c7c5;
  margin-top: 25px;

}
.alert {
  background: #FAE6E6;
  padding: .5em 1em 2em 2em;
  margin: 0 0 0 0;
  box-shadow: inset 6px 0 #CC0000;
}

.pay_fail_text_css {
  font-size: 1.5em;
  font-weight: 700;
  color: black;
  letter-spacing: 0.02em;
  margin-top: 1em;
  margin-left: 2em;
  margin-bottom: .5em;
}

.alert_text {
  margin-left: 3em;
  margin-bottom: .5em;
}
</style>
<link href='http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.min.css'>


<div class ="alert">
    <h4 class="pay_fail_text_css">Payment failure</h4>
    <div class="alert_text">Please correct the information below and try again.</div>
  </div>